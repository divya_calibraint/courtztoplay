﻿using CourtzToPlayDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Interfaces
{
    public interface ICourtRepository
    {
        Task CourtCreation(CourtList courts);
    }
}
