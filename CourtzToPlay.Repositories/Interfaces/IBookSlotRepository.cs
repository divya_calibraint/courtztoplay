﻿using CourtzToPlay.Repositories.Entities;
using CourtzToPlayDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CourtzToPlayDataBase.BookingSlot;

namespace CourtzToPlay.Repositories.Interfaces
{
    public interface IBookSlotRepository
    {
        Task<BookedSlot> BookedSlot(SlotBookingUserInput slotUser);
        //Task<BookingSlot> BookedSlot(int userId, int courtID, int slotno, string timesToPlay, string timesTo);
    }
}
