﻿using CourtzToPlay.Repositories.Entities;
using CourtzToPlay.Repositories.Interfaces;
using CourtzToPlayDataBase;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CourtzToPlayDataBase.BookingSlot;

namespace CourtzToPlay.Repositories.Repositories
{
    public class BookSlotRepository : DBConnectivity, IBookSlotRepository
    {
        public readonly IValidator<BookingSlot> validator;
        public BookSlotRepository(CourtzToPlayEntities courtzTo, IValidator<BookingSlot> validate) : base(courtzTo)
        {
            this.validator = validate;
        }



        /*public async Task<BookingSlot> BookedSlot(int userId, int courtID, int slotno, string startingTime, string endingTime)
        {
            var slotCount = from f in _Courtz.CourtList where f.CourtId == courtID select f.NoOfSlots;
            if (slotCount == null)
                throw new Exception("Court not found");
            var slotLocation = from d in _Courtz.CourtList where d.CourtId == courtID select d.Locations;
            var noOfSlots = slotCount.Single();
            var remainingSlot =  slotCount.Single() - slotno.ToString().Length; // d.Single is used to get only single element
            var location = slotLocation.Single();

            //var j = startingTime.ToString().Length - endingTime.ToString().Length;
            //TimeSpan timing = TimeSpan.Parse("05:00");
            TimeSpan time = TimeSpan.Parse(startingTime);
            TimeSpan timePlay = TimeSpan.Parse(endingTime);
            TimeSpan totalhours = time - timePlay;

            string o = time + "{to}" + timePlay;
            BookingSlot book = new BookingSlot();
            //if (o.Contains(book.arrayTiming))
            _Courtz.BookingSlot.Add(book = new BookingSlot()
            {
                UserIDKey = userId,
                CourtIDKey = courtID,
                Locations = location,
                Noofslots = noOfSlots,
                BookedSlotNo = slotno,
                RemainingSlot = remainingSlot,
                BookedDate = DateTime.UtcNow,
                StartTime = startingTime,
                EndTime = endingTime,
                TotalHour = totalhours
            });
            return book;
        }*/

        public async Task<BookedSlot> BookedSlot(SlotBookingUserInput slotUser)
        {
            var user = from q in _Courtz.Users where q.UserId == slotUser.userId select q.UserId;
            if (user.Count() == 0)
                throw new ExceptionCourt("Invalid user");
            var slotCount = from f in _Courtz.CourtList where f.CourtId == slotUser.courtId select f.NoOfSlots;
            if (slotCount.Count() == 0)
                throw new ExceptionCourt("Invalid Court");
            var slotLocation = from d in _Courtz.CourtList where d.CourtId == slotUser.courtId select d.Locations;
            var noOfSlots = slotCount.Single();
            var remainingSlot = slotCount.Single() - slotUser.slotNo.ToString().Length; // d.Single is used to get only single element
            var location = slotLocation.Single();

            TimeSpan st = TimeSpan.Parse(slotUser.startingTime);
            TimeSpan et = TimeSpan.Parse(slotUser.endingTime);
            var openingTimeDb = from q in _Courtz.CourtList where q.CourtId == slotUser.courtId select q.OpeningTime; // Get opening time from the db by the specific courtId
            var openingTimeFromDB = openingTimeDb.FirstOrDefault(); // Time value will be in openingTimeFromDB variable
            if (openingTimeFromDB > st) 
                throw new Exception("Opening time is invalid");
            /*var al = from q in _Courtz.CourtList where q.CourtId == slotUser.courtId select q.ClosingTime;
            var tt = al.FirstOrDefault();
            if (tt > et)
                throw new Exception("Closing time is invalid");*/
            var totalHours = et.Hours -st.Hours;
            var bookm = new BookingSlot { TotalHour = totalHours };
            var total = validator.Validate(bookm); // validates the totalHours. It must be 1 hour
            if (!total.IsValid)
                throw new Exception("Hour is invalid");

            BookingSlot book = new BookingSlot();
            _Courtz.BookingSlot.Add(book = new BookingSlot()
            {
                UserIDKey = slotUser.userId,
                CourtIDKey = slotUser.courtId,
                Locations = location,
                Noofslots = noOfSlots,
                BookedSlotNo = slotUser.slotNo,
                RemainingSlot = remainingSlot,
                BookedDate = DateTime.UtcNow,
                StartTime = st,
                EndTime = et,
                TotalHour = totalHours
            }); // Adding data to the database
            var startTime = book.StartTime.ToString();
            var endTime = book.EndTime.ToString();
            BookedSlot bookedSlot = new BookedSlot()
            {
                UserIDKey = book.UserIDKey,
                CourtIDKey = book.CourtIDKey,
                Locations = book.Locations,
                Noofslots = book.Noofslots,
                BookedSlotNo = book.BookedSlotNo,
                RemainingSlot = book.RemainingSlot,
                BookedDate = book.BookedDate,
                StartTime = startTime,
                EndTime = endTime,
                TotalHour = book.TotalHour
            };
            await _Courtz.SaveChangesAsync(); // Saving data to the database
            return bookedSlot;
        }
    }
}
