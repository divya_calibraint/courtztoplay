﻿using CourtzToPlay.Repositories.Interfaces;
using CourtzToPlayDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Repositories
{
    public class CourtRepository : DBConnectivity, ICourtRepository
    {
        public CourtRepository(CourtzToPlayEntities playEntities) : base(playEntities)
        {

        }
        public Task CourtCreation(CourtList courts)
        {
            var obj = courts.OpeningTime;
            _Courtz.CourtList.Add(new CourtList()
            {
                CourtName = courts.CourtName,
                Locations = courts.Locations,
                NoOfSlots = courts.NoOfSlots,
                PhoneNo = courts.PhoneNo,
                OpeningTime = new TimeSpan(2,3,3),
                ClosingTime = courts.ClosingTime
            });
            return _Courtz.SaveChangesAsync();
        }
    }
}
