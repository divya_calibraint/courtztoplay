﻿using CourtzToPlay.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CourtzToPlayDataBase;
using System.Security.Cryptography;
using System.Linq.Expressions;

namespace CourtzToPlay.Repositories.Repositories
{
    public class UserRepository : DBConnectivity, IUserRepository
    {
        public UserRepository(CourtzToPlayEntities courtz) : base(courtz)
        {
        }

        public async Task<IEnumerable<User>> GetUsers()
        {
            return _Courtz.Users.ToList();
        }

        public Task CreateUser(User user)
        {
            CreatePasswordHash(user.Passwords, out byte[] passwordHash, out byte[] passwordSalt);
            user.PasswordSalt = passwordSalt;
            user.PasswordHash = passwordHash;
            _Courtz.Users.Add(new User()
            {
                EmailAddress = user.EmailAddress,
                Passwords = null,      
                Username = user.Username,
                PasswordSalt = user.PasswordSalt,
                PasswordHash = user.PasswordHash
            });
            return _Courtz.SaveChangesAsync();
        }

        private static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            var hmac = new System.Security.Cryptography.HMACSHA512();
            passwordSalt = hmac.Key;
            passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
        }
    }
}
