﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Entities
{
    public class CourtListFilters   // Gets all the data from the database to give input to the filter.
    {
        public int CourtId { get; set; }
        public string CourtName { get; set; }
        public int NoOfSlots { get; set; }
        public string PhoneNo { get; set; }
        public string Locations { get; set; }
        public string OpeningTime { get; set; }
        public string ClosingTime { get; set; }
    }
}
