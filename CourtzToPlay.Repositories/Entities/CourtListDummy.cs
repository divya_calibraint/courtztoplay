﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Entities
{
    public class CourtListDummy
    {
        public int CourtId { get; set; }
        public string CourtName { get; set; }
        public string Locations { get; set; }
        public string PhoneNo { get; set; }
        public Nullable<int> NoOfSlots { get; set; }
        public string OpeningTime { get; set; }
        public string ClosingTime { get; set; }
    }
}
