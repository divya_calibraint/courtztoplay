﻿using Nest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Entities
{
    public class CourtSpecification
    {
        public int CourtId { get; set; }
        public string CourtName { get; set; }
        public string Locations { get; set; }
        public string OpeningTime { get; set; }
        public string ClosingTime { get; set; }
    }
}
