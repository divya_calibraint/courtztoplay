﻿using CourtzToPlayDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Entities
{
    public class BookedSlot
    {
        public int BookId { get; set; }
        public int CourtIDKey { get; set; }
        public int UserIDKey { get; set; }
        public System.DateTime BookedDate { get; set; }
        public int Noofslots { get; set; }
        public int BookedSlotNo { get; set; }
        public int RemainingSlot { get; set; }
        public string Locations { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int TotalHour { get; set; }
    }
}
