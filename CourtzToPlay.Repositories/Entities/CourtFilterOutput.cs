﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Entities
{
    public class CourtFilterOutput  // Output result for filters
    {
        public int Count { get; set; }
        public List<CourtListFilters> CourtItem { get; set; }
    }
}
