﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Entities
{
    public class Requirements
    {
        public int CourtID { get; set; }
        public string Location { get; set; }
        public static int NoOFSlots { get; set; }
    }
}
