﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Entities
{
    public class ExceptionCourt : Exception
    {
        public ExceptionCourt(string message) : base(message)
        {
        }
    }
}
