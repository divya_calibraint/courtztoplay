﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Repositories.Entities
{
    public class SlotBookingUserInput
    {
        public int userId { get; set; }
        public int courtId { get; set; }
        public int slotNo { get; set; }
        public string startingTime { get; set; }
        public string endingTime { get; set; }
    }
}
