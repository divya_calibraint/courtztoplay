﻿using CourtzToPlayDataBase;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourtzToPlay.Api.Validator
{
    public class TotalHoursValidator :AbstractValidator<BookingSlot>
    {
        public TotalHoursValidator()
        {
            int i = 1;
            RuleFor(e => e.TotalHour).Must(e => i.Equals(e)).WithMessage("Need perfect time");
        }
    }
}
