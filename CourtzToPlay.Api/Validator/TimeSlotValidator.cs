﻿using CourtzToPlay.Repositories.Entities;
using CourtzToPlayDataBase;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CourtzToPlay.Api.Validator
{
    public class TimeSlotValidator : AbstractValidator<SlotBookingUserInput>
    {
        public TimeSlotValidator()
        {
            List<string> con = new List<string>() { "5:00", "6:00", "7:00", "8:00", "9:00","10:00","11:00", "12:00","13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00" };
            RuleFor(e => e.startingTime)
                .Must(e => con.Contains(e))
                 .WithMessage("Please use correct time");

            /*List<string> end = new List<string>() { "5:00", "6:00", "7:00", "8:00", "9:00", "10:00", "11:00", "12:00", "13:00", "14:00", "15:00", "16:00", "17:00", "18:00", "19:00", "20:00", "21:00", "22:00", "23:00" };
            RuleFor(e => e.endingTime)
                .Must(e => end.Contains(e))
                 .WithMessage("Please use correct time");*/
        }
    }
}
