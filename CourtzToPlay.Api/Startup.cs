using CourtzToPlay.Repositories.Interfaces;
using CourtzToPlay.Repositories.Repositories;
using CourtzToPlay.Services.ServiceInterface;
using CourtzToPlay.Services.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using EntityFrameworkCore.Extensions;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;
using CourtzToPlayDataBase;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using FluentValidation;
using CourtzToPlay.Repositories.Entities;
using CourtzToPlay.Api.Validator;

namespace CourtzToPlay.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddEntityFrameworkSqlServer().AddDbContext<CourtzToPlayEntities>(options => options.UseSqlServer(Configuration.GetConnectionString("DbConnection")));
            services.AddControllers();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CourtzToPlay.Api", Version = "v1" });
            });
            ConfigureClassInterfaceMappings(services);

            services.AddAuthentication(a =>
            {
                a.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                a.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(e => e.Events = new JwtBearerEvents
                {
                    OnTokenValidated = async context =>
                    {
                        if (context.Principal?.Identity?.Name == null)
                        {
                            context.Fail("Unauthorized");
                            return;
                        }

                        context.HttpContext.User = context.Principal;
                        UserService userService = context.HttpContext.RequestServices.GetRequiredService<UserService>();

                        e.SaveToken = true;
                        e.RequireHttpsMetadata = false;
                        e.TokenValidationParameters = new TokenValidationParameters()
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"])),
                            ValidateIssuer = false,
                            ValidateAudience = false
                        };
                    }
                });
        }

        private static void ConfigureClassInterfaceMappings(IServiceCollection services)
        {
            services.AddTransient<IUserRepository, UserRepository>(); // User repository injection

            services.AddTransient<IUserService, UserService>();

            services.AddTransient<ICourtRepository, CourtRepository>();
            services.AddTransient<ICourtService, CourtService>();

            services.AddTransient<IBookSlotRepository, BookSlotRepository>();
            services.AddTransient<IBookSlotService, BookSlotService>();

            services.AddTransient<IValidator<SlotBookingUserInput>, TimeSlotValidator>();
            services.AddTransient<IValidator<BookingSlot>, TotalHoursValidator>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CourtzToPlay.Api v1"));
            }

            app.UseMiddleware(typeof(ExceptionCustomMiddleware));

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseAuthentication();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
