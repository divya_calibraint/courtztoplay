﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CourtzToPlayDataBase;
using CourtzToPlay.Services.Services;
using Microsoft.AspNetCore.Authorization;
using CourtzToPlay.Services.ServiceInterface;

namespace CourtzToPlay.Api.Controllers
{
    [Route("api")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService userService;
        public UserController(IUserService _userService)
        {
            userService = _userService;
        }

        /// <summary>
        /// Create User
        /// </summary>
        /// <param name="user"></param>
        /// <remarks>Insert new user</remarks>
        /// <returns>User will be created and user data will be saved in database</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("UserCreation")]
        public  Task CreateUser([FromBody] User user)
        {
            return userService.UserCreation(user);
        }

        /// <summary>
        /// Login
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        /// <returns>Created user will be logged in</returns>
        [AllowAnonymous]
        [HttpPost]
        [Route("Login")]
        public async Task<User> LoginUser(string email, string password)
        {
            return await userService.Login(email, password);
        }
    }
}
