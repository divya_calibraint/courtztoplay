﻿using CourtzToPlay.Repositories.Entities;
using CourtzToPlay.Services.ServiceInterface;
using CourtzToPlay.Services.Services;
using CourtzToPlayDataBase;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CourtzToPlay.Api.Controllers
{
    [Route("api")]
    [ApiController]
    public class CourtListController : ControllerBase
    {
        private readonly ICourtService courtService;

        public CourtListController(ICourtService court)
        {
            courtService = court;
        }

        /*[Route("CourtCreation")]  // admin role
        [HttpPost]
        public IActionResult CreateCourt([FromBody] CourtList court)
        {
            var courtCreated = courtService.CourtManagerSpecification(court);
            return Ok(courtCreated);
        }*/

        /// <summary>
        ///  Gets the list of court
        /// </summary>
        /// <returns>courtList</returns>
        [Route("GetCourtList")]
        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetCourtList()
        {
            var courtList = courtService.GetCourts();
            if (courtList == null)
                return BadRequest(courtList);
            else
                return Ok(courtList);
        }

        [Route("GetCourtListWithFilter")]
        [HttpPost]
        [ProducesResponseType(typeof(CourtFilterOutput), (int)HttpStatusCode.OK)]
        [AllowAnonymous]
        public IActionResult Filters(FilteringData filteringData)
        {
            try
            {
                var courts = courtService.GetFilter(filteringData);

                if (courts == null)
                    return BadRequest();

                var output = new CourtFilterOutput()
                {
                    Count = courts.Count,
                    CourtItem = courts.Select(m => CourtService.CourtList(m)).ToList()
                };

                if (output == null)
                    return BadRequest();
                else
                    return Ok(output);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message.ToString());
            }
        }

        [Route("GetSpecificCourt")]
        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(typeof(CourtListDummy), (int)HttpStatusCode.OK)]
        public IActionResult GetSpecificCourt(int CourtId)
        {
            var specificCourt = courtService.GetSpecificCourtDetails(CourtId);
            return Ok(specificCourt);
        }
    }
}
