﻿using CourtzToPlay.Repositories.Entities;
using CourtzToPlay.Services.ServiceInterface;
using CourtzToPlayDataBase;
using FluentValidation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static CourtzToPlayDataBase.BookingSlot;

namespace CourtzToPlay.Api.Controllers
{
    [Route("api")]
    [ApiController]
    public class BookSlotController : ControllerBase
    {
        public readonly IBookSlotService bookSlot;
        public readonly IValidator<SlotBookingUserInput> validator;
        public BookSlotController(IBookSlotService slotService, IValidator<SlotBookingUserInput> validate)
        {
            bookSlot = slotService;
            this.validator = validate;
        }

        [HttpPost]
        [Route("BookASlot")]
        public async Task<IActionResult> BookingSlot(SlotBookingUserInput slotUser)
        {
            var bookedSlot = await bookSlot.BookASlot(slotUser);
            return Ok(bookedSlot);
        }
    }
}
