﻿using CourtzToPlay.Repositories.Entities;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace CourtzToPlay.Api
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class ExceptionCustomMiddleware
    {
        private readonly RequestDelegate _next;

        public ExceptionCustomMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch(Exception ex)
            {
                await ExceptionHandler(httpContext, ex);
            }
            
        }

        private Task ExceptionHandler(HttpContext httpContext, Exception ex)
        {
            var errors = new ErrorDetails { Message = ex.Message };
            var errormsg = JsonConvert.SerializeObject(errors);

            httpContext.Response.ContentType = "application/json";
            httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;

            return httpContext.Response.WriteAsync(errormsg);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class ExceptionCustomMiddlewareExtensions
    {
        public static IApplicationBuilder UseExceptionCustomMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionCustomMiddleware>();
        }
    }
}
