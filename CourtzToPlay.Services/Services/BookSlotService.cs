﻿using CourtzToPlay.Repositories.Entities;
using CourtzToPlay.Repositories.Interfaces;
using CourtzToPlay.Services.ServiceInterface;
using CourtzToPlayDataBase;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CourtzToPlayDataBase.BookingSlot;

namespace CourtzToPlay.Services.Services
{
    public class BookSlotService : IBookSlotService
    {
        public readonly IBookSlotRepository book;
        public readonly IValidator<SlotBookingUserInput> validator;
        public BookSlotService(IBookSlotRepository bookSlot, IValidator<SlotBookingUserInput> validate)
        {
            book = bookSlot;
            this.validator = validate;
        }
        public Task<BookedSlot> BookASlot(SlotBookingUserInput slotUser)
        {
            var timing = new SlotBookingUserInput { startingTime = slotUser.startingTime, endingTime = slotUser.endingTime };
            var e = validator.Validate(timing);
            if (!e.IsValid)
            {
                throw new ExceptionCourt("Timing is invalid");
            }
            if (slotUser.userId <= 0)
                throw new ExceptionCourt("User ID invalid");
            if (slotUser.courtId <= 0)
                throw new ExceptionCourt("Court ID is invalid");
            if (slotUser.slotNo == 0)
                throw new ExceptionCourt("Slot not selected");
            return book.BookedSlot(slotUser); 
        }
    }
}
