﻿using CourtzToPlay.Repositories.Interfaces;
using CourtzToPlay.Repositories.Repositories;
using CourtzToPlay.Services.ServiceInterface;
using CourtzToPlayDataBase;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Services.Services
{
    public class UserService : DBConnectivity, IUserService
    {
        public readonly IUserRepository userRepository;
        public readonly IConfiguration configuration;
        public readonly CourtzToPlayEntities courtzToPlayEntities;
       
        public UserService(IUserRepository _userRepository, IConfiguration _configuration, CourtzToPlayEntities courtzTo) : base(courtzTo)
        {
            this.userRepository = _userRepository;
            configuration = _configuration;
            courtzToPlayEntities = courtzTo;
        }
        public Task<IEnumerable<User>> GetUsers()
        {
            return userRepository.GetUsers();
        }

        public async Task<User> Login(string email,string password)
        {
            User loggedUser = await Authenticate(email, password);
            return loggedUser;
        }

        public async Task<User> Authenticate(string email, string password)
        {
            if (string.IsNullOrEmpty(email) && string.IsNullOrWhiteSpace(email))
                throw new Exception("Need correct email");
            if (string.IsNullOrWhiteSpace(password) && string.IsNullOrEmpty(password))
                throw new Exception("Password mismatch");

        
            var userData = courtzToPlayEntities.Users.Where(e => e.EmailAddress == email).FirstOrDefault();
            if (userData == null)
                throw new Exception("User Not Found");
            User userAuthenticated = userData;
            if (!userAuthenticated.VerifyPasswordHash(password))
                throw new Exception("authenticationFail");

            userAuthenticated.CleanUser();

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Key"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            var tokenCreation = new JwtSecurityToken(configuration["Jwt:issuer"],
                                             configuration["Jwt:issuer"],
                                             null,
                                             expires: DateTime.UtcNow.AddDays(4),
                                             signingCredentials: credentials);
            var token = new JwtSecurityTokenHandler().WriteToken(tokenCreation);
            userAuthenticated.Token = token;
            return userAuthenticated;
        }

        public Task UserCreation(User user)
        {
            if (string.IsNullOrWhiteSpace(user.Passwords))
                throw new Exception("Password needed");
            if (string.IsNullOrWhiteSpace(user.EmailAddress))
                throw new Exception("EmailNull");
            if (string.IsNullOrWhiteSpace(user.Username) && string.IsNullOrEmpty(user.Username))
                throw new Exception("Username necessary");
            return userRepository.CreateUser(user);
        }
    }
}
