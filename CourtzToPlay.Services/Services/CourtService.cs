﻿using CourtzToPlay.Repositories.Entities;
using CourtzToPlay.Repositories.Interfaces;
using CourtzToPlay.Services.ServiceInterface;
using CourtzToPlayDataBase;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Services.Services
{
    public class CourtService : DBConnectivity, ICourtService
    {
        public readonly ICourtRepository courtRepository;
        public readonly CourtzToPlayEntities courtzToPlay;
        public readonly IConfiguration iconfiguration;

        public CourtService(CourtzToPlayEntities courtzTo, IConfiguration configuration, ICourtRepository courtRepo) : base(courtzTo)
        {
            courtzToPlay = courtzTo;
            iconfiguration = configuration;
            courtRepository = courtRepo;
        }

        public List<CourtSpecification> GetCourts()
        {
            var courts = courtzToPlay.CourtList.Select(e => new CourtSpecification()
            {
                CourtName = e.CourtName,
                CourtId = e.CourtId,
                Locations = e.Locations,
                OpeningTime = e.OpeningTime.ToString(@"hh\:mm"),
                ClosingTime = e.ClosingTime.ToString(@"hh\:mm")
            }).ToList();
            return courts.ToList();
        }

        public List<CourtList> GetFilter(FilteringData filtering)
        {
            if (filtering.FilterBy == "")
                throw new ExceptionCourt("Filter not added");
            try
            {
                var filters = filtering.FilterBy.Trim().ToLower();  // Gets the filter specification make it lowercase.

                var courts = courtzToPlay.CourtList.Where(c => c.CourtName.Trim().ToLower() == filters).AsQueryable();

                if (!string.IsNullOrEmpty(filters))
                {
                    courts = courtzToPlay.CourtList.Where(m => m.CourtName.Trim().ToLower().Contains(filters)
                    || m.Locations.Trim().ToLower().Contains(filters)); // Checks whether database data which contains the specified filter is in database.
                }
                return courts.ToList();
            }
            catch
            {
                throw new Exception("No filters are specified");
            }
        }

        public static CourtListFilters CourtList(CourtList courtList)
        {
            var openTiming = courtList.OpeningTime.ToString(@"hh\:mm");
            var closeTiming = courtList.ClosingTime.ToString(@"hh\:mm");
            return new CourtListFilters()
            {
                CourtId = courtList.CourtId,
                CourtName = courtList.CourtName,
                NoOfSlots = (int)courtList.NoOfSlots,
                PhoneNo = courtList.PhoneNo,
                Locations = courtList.Locations,
                OpeningTime = openTiming,
                ClosingTime = closeTiming
            };
        }

        public static CourtListDummy CourtListData(CourtList courtList)
        {
            var openTiming = courtList.OpeningTime.ToString(@"hh\:mm");
            var closeTiming = courtList.ClosingTime.ToString(@"hh\:mm");
            return new CourtListDummy()
            {
                CourtId = courtList.CourtId,
                CourtName = courtList.CourtName,
                NoOfSlots = (int)courtList.NoOfSlots,
                PhoneNo = courtList.PhoneNo,
                Locations = courtList.Locations,
                OpeningTime = openTiming,
                ClosingTime = closeTiming
            };
        }




        public List<CourtListDummy> GetSpecificCourtDetails(int CourtID)
        {
            var courtList = courtzToPlay.CourtList.Where(e => e.CourtId == CourtID).AsQueryable();
            List<CourtListDummy> specificCourt = new List<CourtListDummy>();
            specificCourt = courtList.Select(m => CourtListData(m)).ToList();

            if (specificCourt.Count() == 0)
                throw new ExceptionCourt("Court not found");
            else
                return specificCourt.ToList();
        }

        public async Task CourtManagerSpecification<T>(CourtList courtManage)
        {
            if (courtManage != null)
                try
                {
                    await courtRepository.CourtCreation(courtManage);
                }
                catch (Exception e)
                {
                    throw new Exception("Need court specification", e);
                }
            //return courtRepository.CourtCreation(courtManage);
        }
    }
}
