﻿using CourtzToPlay.Repositories.Entities;
using CourtzToPlayDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static CourtzToPlayDataBase.BookingSlot;

namespace CourtzToPlay.Services.ServiceInterface
{
    public interface IBookSlotService
    {
        Task<BookedSlot> BookASlot(SlotBookingUserInput slotUser);
    }
}
