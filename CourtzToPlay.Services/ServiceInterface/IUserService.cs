﻿using CourtzToPlayDataBase;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CourtzToPlay.Services.ServiceInterface
{
    public interface IUserService
    {
        Task<IEnumerable<User>> GetUsers();
        Task UserCreation(User user);
        Task<User> Login(string email,string password);
        Task<User> Authenticate(string email, string password);
    }
}
