﻿using CourtzToPlay.Repositories.Entities;
using CourtzToPlayDataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CourtzToPlay.Services.ServiceInterface
{
    public interface ICourtService
    {
        Task CourtManagerSpecification<T>(CourtList courtManage);
        List<CourtSpecification> GetCourts();
        List<CourtList> GetFilter(FilteringData filtering);
        List<CourtListDummy> GetSpecificCourtDetails(int CourtId);
    }
}
